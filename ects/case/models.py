from django.db  import models
import datetime
from django.core.urlresolvers import reverse
from django.contrib.auth.models  import User
from django.db.models.signals  import  post_save
from django.dispatch import  receiver
from utils import CharNullField
from django.core.exceptions import ValidationError

class RootModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Region(RootModel):
    code = models.CharField(max_length=200,unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class District(RootModel):
    code = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    region = models.ForeignKey(Region,on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name  = 'Constituency'
        verbose_name_plural  = 'Constituencies'


class Town(RootModel) :
    name = models.CharField(max_length=200)
    district = models.ForeignKey(District,on_delete=models.CASCADE,related_name="towns")

    def __str__(self):
        return self.str



class NationaIdType(RootModel):
    type = models.CharField(max_length=200)




class OffenceCategory(RootModel):
    category  = models.CharField(max_length=250)


    def __str__(self):
        return  self.category

    class Meta:
        verbose_name  = 'Category of Offence'
        verbose_name_plural  = 'Category Of Offences'


class Offence(RootModel):
    offence = models.CharField(max_length=250)
    category = models.ManyToManyField(OffenceCategory,default=None,blank=True,null=True)

    def  __str__(self):
        return self.offence


class InvestigativeBody(RootModel):
    code = models.CharField(max_length=5, null=False,blank=False)
    name  = models.CharField(max_length=200,null=False,blank=False)

    def __str__(self):
        return self.name


class Station(RootModel):
    code = models.CharField(max_length=12   ,blank=True,null=True)
    investigative_body  = models.ForeignKey( InvestigativeBody, null=True,blank=True)
    name = models.CharField(max_length=25 ,blank=True,null=True)
    email_address =  models.EmailField(blank=True,null=True)
    phone_number  = models.CharField(max_length=20,null=True ,blank=True)
    region = models.ForeignKey(Region,null=True,blank=True)

    #Do we distinguish between an EOCO or BNI at Tamale and Takoradi?
    def  __str__(self):
            return " %s ( %s )"  %(self.name,self.region)

    class Meta:
        verbose_name  = 'Investigative Station'
        verbose_name_plural  = 'Investigative Station'


class DocketStatus(RootModel):
    status  = models.CharField(max_length=100,null=False,blank=False)

    class Meta:
        verbose_name = 'Doket Status'
        verbose_name_plural = 'Doket Status'

    def __str__(self):
        return self.status


class AppealStatus(RootModel):
    status  = models.CharField(max_length=100,null=False,blank=False)

    class Meta:
        verbose_name = 'Appeals Status'
        verbose_name_plural = 'Appeal Status'

    def __str__(self):
        return self.status


class MotionStatus(RootModel):
    status  = models.CharField(max_length=100,null=False,blank=False)

    class Meta:
        verbose_name = 'Motion Status'
        verbose_name_plural = 'Motion Status'

    def __str__(self):
        return self.status


class PetitionStatus(RootModel):
    status  = models.CharField(max_length=100,null=False,blank=False)

    class Meta:
        verbose_name = 'Petition Status'
        verbose_name_plural = 'Petition Status'

    def __str__(self):
        return self.status


class NatureOfMotion(RootModel):
    nature = models.CharField(max_length=500,null=False,blank=False)

    def __str__(self):
        return self.nature

class AGStaff(RootModel):
    SECRETARY = 1
    EXECUTIVE_OFFICER = 2
    ADMINISTRATIVE_OFFICER = 3
    STATE_ATTORNEY = 4
    REGIONAL_HEAD = 5
    DPP = 6

    ROLE_CHOICES = (
            (SECRETARY ,'Secretary'),
            (EXECUTIVE_OFFICER ,'Executive Officer'),
           (ADMINISTRATIVE_OFFICER , 'Administrative Officer'),
            (STATE_ATTORNEY , 'State Attorney'),
            (REGIONAL_HEAD , 'Regional Head'),
            (DPP,'Director of Public Prosecution'),

        )

    STATUS_CHOICES = (
            ('active','Active'),
            ('disabled','Disabled')
        )

    UNIT_CHOICES = (
            ('secretary' ,'Secretary'),
            ('executive_officer' ,'Executive Officer'),
           ('administrative_officer' , 'Administrative Officer'),
            ('state_Attorney' , 'State Attorney'),
            ('regional_head', 'Regional Head'),
            ('DPP','Director of Public Prosecution'),
        )


    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200,blank=False,null=False)
    phone_number  = models.CharField(max_length=20,null=True)
    email_address  = models.EmailField(null=True,blank=True)
    staff_id = CharNullField(max_length=20,null=True, blank=True)
    region = models.ForeignKey(Region,null=True,blank=True)
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES,null=True,blank=True)
    status  = models.CharField(max_length=20,choices=STATUS_CHOICES,null=False,blank=False )
    unit  = models.CharField(max_length=25,choices=UNIT_CHOICES,null=False,blank=False )

    def __str__(self):
        return   "%s  %s "  %(self.firstname  ,  self.lastname)

#Make this  A Proxy Model
class StateAttorney(RootModel):
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200,blank=True,null=True)
    phone_number  = models.CharField(max_length=20,null=True)
    email_address  = models.EmailField(null=True,blank=True)
    staff_id = CharNullField(max_length=20,null=True, blank=True)
    region = models.ForeignKey(Region,null=True,blank=True)

    def __str__(self):
        return   "%s  %s"  %(self.firstname,self.lastname)



class DocketReceived(RootModel):
    title = CharNullField(max_length=50,blank=True,null=True)
    registered_offense_number = models.CharField(max_length=50,null=True, default=None,blank=True)
    offence = models.ForeignKey(Offence,on_delete=models.CASCADE)
    received_by = models.ForeignKey(AGStaff,null=True, blank=True)
    received_from = models.CharField(max_length=50,null=True, default=None,blank=True)
    date_received = models.DateField()
    date_recorded = models.DateField()
    region = models.ForeignKey(Region)

    def __str__(self):
        return  "%s (%s)" %(self.title, self.registered_offense_number)

    class Meta:
            verbose_name  = 'Docket Received'
            verbose_name_plural  = 'Docket Received'


class Investigator(RootModel):
    badge_number = models.CharField(max_length=50,unique=True,null=True)
    title = models.CharField(max_length=200)
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    phone_number  = models.CharField(max_length=20,null=True,blank=True)
    email = models.EmailField(blank=True,null=True)
    station = models.ForeignKey(Station,null=True, blank=True,default=None)

    def   __str__(self):
        return   "%s %s  %s (%s-%s)"  %(self.title,self.firstname,self.lastname,self.station, self.badge_number)

class Docket(RootModel):
    title = CharNullField(max_length=500,null=False, )
      #number of referrence letter from police or any referring agency
    registered_offense_number = models.CharField(max_length=50,null=True, default=None,blank=True)

     #Assigned by AG
    docket_number  = CharNullField(max_length=250,unique=True,blank=True)
    status  = models.ForeignKey(DocketStatus,on_delete=models.CASCADE)
    region = models.ForeignKey(Region,on_delete=models.CASCADE)
    #this is the same as RON if its a police station
    station_reference = models.CharField(max_length=250,blank=True,default=None)
    #investigator  = models.ForeignKey(Investigator,blank=True, null=True)
    date_of_submission = models.DateField(blank=True,null=True)
    date_of_charge_sheet  = models.DateField(blank=True , null=True)

    attorneys = models.ManyToManyField(StateAttorney,through = 'AssignedDockets')
    investigator = models.ManyToManyField(Investigator,blank=True)

    def __str__(self):
        return self.title


#intermediary
class AssignedDockets(RootModel):
    attorney = models.ForeignKey(StateAttorney)
    docket= models.ForeignKey(Docket)
    date_assigned  = models.DateField(null=True,blank=True)
    date_received  = models.DateField(null=True,blank=True)
    remarks =models.CharField(max_length=500, null=True,blank=True)

    class Meta:
        verbose_name =  'Attorneys Assigned'
        verbose_name_plural = 'Attorneys Assigned'

    def __str__(self):
        return  "%s  %s (%s )"  %(self.attorney.firstname, self.attorney.lastname , self.attorney.region)




#Docket Assignment Book?
#Document Movement Book?
#Document Type?
#Document Reference Number?
class DocketMovement(RootModel):
    docket = models.ForeignKey(Docket,on_delete = models.CASCADE)
    from_whom   = models.ForeignKey(AGStaff,related_name = 'sender')
    to_whom  = models.ForeignKey(AGStaff,related_name='receiver')
    remarks   = models.CharField(max_length=250,null=True,blank=True)
   # region = models.ForeignKey(Region)
    date = models.DateField()


    class Meta:
        verbose_name = 'Doket  Movement'
        verbose_name_plural = 'Doket  Movements'

    def  __str__(self):
        return  "%s  sent to  %s from %s on % s" %(docket.title)

class DocketDispatch(RootModel):
    docket = models.ForeignKey(Docket)
    subject  = models.CharField(max_length=200)
    registry_no  = models.CharField('Dispatch Reference',max_length=250)
    date_of_dispatch = models.DateField(null=True,blank=True)
    #similar to docket number?
    officer = models.CharField('Dispatch Officer',max_length=700,blank=True,null=True)
    date_of_letter = models.DateField(null=True , blank=True)
    region = models.ForeignKey(Region,null=True,blank=True)

    def __str__(self):
        return "%s  (%s) "  %(self.docket,self.registry_no)


#M2M Docket + Investigator
#DocketInvestigator(RootModel):
class Case(RootModel):
    docket = models.ForeignKey(Docket,blank=True,null=True,on_delete=models.CASCADE)
    offence = models.ForeignKey(Offence, blank=True,default=None,null=True)
    remarks = models.CharField(max_length=250,blank=True,null=True,default=None)
    date_of_charge_sheet  = models.DateField()
    case_completed = models.BooleanField(null=False,default=False)
    date_completed  = models.DateField(blank=True, null=True,default=None)


class AccusedType(RootModel):
    type  = models.CharField(max_length=100,null=False,blank=False)

    class Meta:
        verbose_name = 'Accused Type'
        verbose_name_plural = 'Accused Type'

    def __str__(self):
        return self.type


class Accused(RootModel):
    GENDER_CHOICES =  (('male','Male'),('female','Female'))
    MARITAL_STATUS_CHOICES = (
        ('married','Married'),
        ('single','Single'),
        ('divorced','Divorced')
        )

    firstname = models.CharField(max_length=350)
    lastname = models.CharField(max_length=250)
    #gender = models.ForeignKey(Gender,null=True,blank=True,default=None,related_name="accused_gender")
    gender  = models.CharField(max_length=20,choices=GENDER_CHOICES,null=True,blank=True)
    dob = models.DateField(blank=True,null=True,default=None)
    marital_status = models.CharField(max_length=20,choices =MARITAL_STATUS_CHOICES,null=True,blank=True, )
    #contact details
    mobile = models.CharField(max_length=20,null=True,blank=True)
    email = models.EmailField(null=True,default=None,blank=True)
    #case = models.ForeignKey(Case , on_delete=models.CASCADE)
    docket  = models.ForeignKey(Docket,blank=True,null=True, on_delete=models.CASCADE)
    accused_type  = models.ForeignKey(AccusedType,blank=True,null=True, on_delete=models.CASCADE)
    offence = models.ManyToManyField(Offence)


    def __str__(self):
        return "%s  %s" %(self.firstname,self.lastname)


class Advice(RootModel):
    docket = models.ForeignKey(Docket,)
    action_taken = models.TextField(null=True,blank=True)
    attorney  = models.ManyToManyField(StateAttorney, blank=True)
    dispatch_date = models.DateField(null=True,blank=True)
    region = models.ForeignKey(Region,null=True,blank=True)

    def __str__(self):
        return self.docket.title


class Motion(RootModel):
    title_of_case = models.CharField(max_length=200)
    file_number = models.CharField(max_length=20)
    nature = models.ForeignKey(NatureOfMotion,blank=True,null=True)
    date_of_service  = models.DateTimeField('Date of Service',blank=True,null=True)
    suit_no = models.CharField('Suit Number',max_length=30,blank=True, null=True)
    offence = models.ManyToManyField(Offence , blank=True)
    attorneys  = models.ManyToManyField(StateAttorney,through='AssignedMotions')
    status = models.ForeignKey(MotionStatus,null=True,blank=True)
    region = models.ForeignKey(Region)


    def __str__(self):
        return " %s   (%s)" %(self.title_of_case, self.suit_no)

'''
    def get_absolute_url(self):
        return  reverse('motions-details',kwargs={'pk' : self.id})
'''

#itermediary
class AssignedMotions(RootModel):
    motion = models.ForeignKey(Motion)
    attorney = models.ForeignKey(StateAttorney)
    date_assigned  = models.DateField(null=True,blank=True)
    date_received  = models.DateField(null=True,blank=True)
    remarks =models.TextField(null=True,blank=True)


class Appeal(RootModel):
    title_of_case = models.CharField(max_length = 300)
    file_number  =  models.CharField(max_length=50, unique=True,null=False,blank=False)
    suit_number  = models.CharField(max_length=200,null=False,blank=False,unique=True)
    offence = models.ForeignKey(Offence, null=True,blank=True)
    attorney  = models.ForeignKey(StateAttorney,null=True,blank=False)
    date_received  =  models.DateField(blank=True,null=True)
    date_assigned  =  models.DateField(blank=True,null=True)
    date_closed  =  models.DateField(blank=True,null=True)
    status  = models.ForeignKey(AppealStatus,null=True,blank=True)
    region = models.ForeignKey(Region,null=True,blank=True)

    def  __str__(self):
        return   self.title_of_case

class GeneralCorrespondence(RootModel):
    file_number  = models.CharField(max_length=50, null=True,blank=True)
    reference = models.CharField(max_length=1500,blank=True, null=True)
    officer = models.CharField(max_length=700,blank=True,null=True)
    subject = models.CharField(max_length =700,null=True)
    comments = models.TextField(blank=True,null=True)
    date_received  =  models.DateField(blank=True,null=True)
    date_on_letter  =  models.DateField(blank=True,null=True)
    date_dispatched =  models.DateField(blank=True,null=True)
    region = models.ForeignKey(Region)


    def __str__(self):
        return self.subject

    class Meta:
        verbose_name = 'General Correspondence'
        verbose_name_plural = 'General Correspondences'


class PardonsAndExtradiction(RootModel):
    file_number  =  models.CharField('File Number',max_length=20,null = False,unique=True,blank=False)
    title_of_case  = models.CharField(max_length=500,null=False,blank=False)
    date_received = models.DateField(null=True,blank=True)
    date_assigned  = models.DateField(null=True,blank=True)
    attorney  = models.ManyToManyField(StateAttorney, blank=True,null=True)
    date_dispatched = models.DateField(null=True,blank=True)
    remarks  =  models.TextField(null=True,blank=True)
    region = models.ForeignKey(Region,on_delete=models.CASCADE,null=True,blank=True)


    class Meta:
        verbose_name = 'Pardons And Extradictions'
        verbose_name_plural = 'Pardons And Exradictions'


    def __str__(self):
        return "%s  (%s)" %(self.subject , self.file_number)


class Petition(RootModel):
    region = models.ForeignKey(Region,on_delete=models.CASCADE)
    title = CharNullField(max_length=800,null=False)
    file_number  = CharNullField(max_length=800, blank=True, null=True)
    attorney = models.ForeignKey(StateAttorney,default=None,blank=True,null=True )
    status  = models.ForeignKey(PetitionStatus,on_delete=models.CASCADE)
    region = models.ForeignKey(Region,on_delete=models.CASCADE,null=True,blank=True)
   # date_received = models.DateField(null=True,blank=True)
   # date_closed = models.DateField(null=True,blank=True)

    def __str__(self):
        return self.title



'''
Date of submission of docket
Which agency submitted the docket
Title and R.O number
Kind of offence

Reference number of the agency or Police
Reference number assigned in AGs Department
Which police station is handling the docket
Name and phone number of the investigator

State of the case if the case is already in court
Name of the court
Name of the Judge

Current updates

Name of attorney handling the case
Date of assignment
Date of advice
Date of dispatch
Date of charge sheet filed
Court assigned
Date of committal
Any processes in the case if already in court

Date case completed


Ability to be updated from any location
Ability to generate statistics according to offences
interface for international co-operation, mutual legal assistance extraditions


CONFIGURATION DATA
INVESTIGATIVE_BODY
INVESTIGATIVE_AGENCY
INVESTIGATOR
OFFENSE_CATEGORY
OFFENSE
SYSTEM _USER_CATEGORY
SYSTEM USER
AG_STAFF (TAKEN UP BY PROFILE)
STATE ATTORNEY


DOCKET_STATUS
APPEAL STATUS
MOTION_STATUS
PETITION_STATUS


ROLE
    ATTORNEY
    SECTION HEAD
    REGIONAL HEAD
    ACTING REGIONAL HEAD
    EXECUTVE OFFICER
    REGIONAL ADMINISTRATOR
    NATIONAL USER
    NATIONAL ADMIN


DOCKET


APPEALS
FILE NUMBER
TITLE OF CASE
SUIT NUMBER
ATTORNEY
OFFENCE
DATE RECEIVED
DATE ASSIGNED
STATUS


PARDONS AND EXTRADITION
FILE NUMBER
TITLE OF CASE
DATE ASSIGNED
ATTORNEY
DATE DISPATCHED
REMARKS


GENERAL_CORRESPONDENCE
FILE NUMBER
DATE RECEIVED
DATE ON LETTER (RECEIVED)
REFERENCE NUMBER OF LETTER RECEIVED (IF ANY)
OFFICER
SUBJECT
COMMENTS


MOTIONS
DATE RECEIVED BY OFFICE
OFFENCE
NATURE OF MOTION
STATUS
DATE OF OUTCOME
DATE OF SERVICE/RECEIPT
TIME RECEIVED
DATE RECEIVED SHOULD GO WITH TIME
ATTORNEY
DATE FORMAT SHOULD BE DD/MM/YY
SUIT NO: FIELD SHOULD TAKE AT LEAST 15 CHARACTERS AND NOT LIMITED TO 10 CHARACTERS


ADVICES
TITLE OF DOCKET
ADVICE DATE
DISPATCHED DATE
ACTION TAKEN INSTEAD
NAME OF ATTORNEY HANDLING CASE

PETITION
    REGION
    TITLE
    RON
    DOCKET_NUMBER
    INVESTIGATIVE_AGENCY
    INVESTIGATIVE_AGENCY_REFERENCE
    ATTORNEY
    DATE_OF_SUBMISSION
    DATE_OF_CHARGE_SHEET


REVISIONS
TABLE - Table to which record was added/modified
FIELD - Field that was modified.
PK_RECORD - Primary Key of the model instance that was modified.
OLD_VAL - Old Value of the field.
NEW_VAL - New Value of the field.
CHANGED_ON - Date it was changed on.
CHANGED_BY - Who changed it?
REVISION_ID - Revision ID of the current Model Instance.


from django.db.models.signals import pre_save
from django.dispatch import receiver
from myapp.models import MyModel

@receiver(pre_save, sender=MyModel)
def my_handler(sender, instance=None, **kwargs):
    # instance variable will have the record which is about to be saved.
    # So log your details accordingly.


class Petition(RootModel):
    status  = models.ForeignKey(DocketStatus,on_delete=models.CASCADE)
    region = models.ForeignKey(Region,on_delete=models.CASCADE)
    title = CharNullField(max_length=500,null=False, )
    #number of referrence letter from police or any referring agency
    registered_offense_number = models.CharField(max_length=50,null=True, default=None,blank=True)
    #Assigned by AG
    docket_number  = CharNullField(max_length=250,unique=True,blank=True)
    #A police station , EOCO , EPA etc
    Station = models.ForeignKey(Station,null=True, blank=True,default=None)
    #this is the same as RON if its a police station
    Station_reference = models.CharField(max_length=250,blank=True,default=None)
   # station = models.ForeignKey(PoliceStation,null=True,blank=True)
    investigator  = models.ForeignKey(Investigator,blank=True, null=True)
    date_of_submission = models.DateField(blank=True,null=True)
    date_of_charge_sheet  = models.DateField(blank=True , null=True)

    attorneys = models.ManyToManyField(StateAttorney,through = 'AssignedDockets')

    def __str__(self):
        return self.title





Prosecutions
    docket
    atttorney
    judge**

ProsecutionJudge
    presecution
    judge


class  Plaintiff(RootModel):
    pass


class Withness(RootModel):
    pass


class Status(RootModel):
    pass


class CourtType(RootModel):
    name  = models.CharField(max_length=200)

    def __str__(self):
        return  self.name

class Court(RootModel):
    name = models.CharField(max_length=200)
    type  = models.ForeignKey(CourtType,on_delete=models.CASCADE)
    #do we need town / districtcs destinction of courts ?
    #How do we intend to distinguish between Accra high court and kumasi high court ?

    def  __str__(self):
        return self.name


class HearingType(RootModel):
    name = models.CharField(max_length= 250,default='first')
    description  = models.CharField(max_length=250,null=True,blank=True,default=None)


class Hearing(RootModel):
    court = models.ForeignKey(Court,default=None,blank=True,null=True)
    hearing_type = models.ForeignKey(HearingType,default=None, blank=True,null =True)
    case  = models.ForeignKey(Case,null=True, default=None,blank=True)
    Judge = models.ForeignKey(Judge,null=True,default=None,blank=True)
    prosecutor = models.ForeignKey(StateAttorney,default= None, blank=True, null=True)
    police = models.ForeignKey(Police,default= None, blank=True , null=True)
    notes = models.TextField(default=None)


class Sentencing(RootModel):
    fine_amount = models.DecimalField( max_digits=20,decimal_places=2,blank=True,null=True)
    sentence_start_date = models.DateField(blank=True,null=True)
    sentence_end_date = models.DateField(blank=True,null=True)
    case_appealed  = models.BooleanField(default=False)
    case_appeal_granted = models.BooleanField(default=False)
    appeal_date = models.DateField(blank=True, null=True)



class Judge(RootModel):
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    phone_number  = models.CharField(max_length=20,null=True)
    email_address  = models.EmailField(null=True,blank=True)

class PoliceInvestigator(RootModel):s
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    phone_number  = models.CharField(max_length=20,null=True)

class PoliceProsecutor(RootModel):
    pass

CREATE TABLE `TABLE` (
    `ID` INTEGER PRIMARY KEY,
    `DATE RECEIVED` VARCHAR(255),
    `FILE NUMBER` VARCHAR(255),
    `NAME` VARCHAR(255),
    `SUIT NUMBER` VARCHAR(255),
    `OFFENCE` VARCHAR(255),
    `ATTORNEY` VARCHAR(255),
    `RECOMMENDATION` VARCHAR(255),
    `DATE CLOSED` VARCHAR(255))


    party (plaintiff,defendant, accused,withness)
    party_category
    party_category_type (1st ,2nd, 3rd,)
    offence (Rape, Careless Driving , )
    offence_category(environmental,corruption,sexual,motor)


OFFENSE / CAUSE OF ACTION
ABDUCTION
ABETMENT OF A CRIME
ABETMENT OF MURDER
ABETMENT OF SUICIDE
ABORTION OR MISCARRIAGE
ASSAULT ON PUBLIC OFFICER
ATTEMPT TO COMMIT A CRIME
ATTEMPT TO COMMIT MURDER
BATTERY
BIGAMY
BREACHES OF PEACE
CARRYING OFFENSIVE WEAPONS
CAUSING HARM
CAUSING HEATH
CHILD RAPE
COMMERCIAL FRAUD
COMPULSION OF MARRIAGE
CONSPIRACY
CORRUPTION
COUNTERFEITING
CRUELTY TO ANIMALS
DECEIVING A PUBLIC OFFICER
DEFAMATORY
DEFAMATORY MATTER
DEFILEMENT
DEFRAUDING BY FALSE PRETENSE
DESTRUCTION OF ANIMAL
DESTRUCTION OF STRAY DOGS
DISHONESTLY RECEIVING
DISHONST APPROPRIATION
DISTURBING THE PEACE IN A PUBLIC PLACE
DOMESTIC VIOLENCE
DRUMMING NEAR COURT DURING SITTING
EMOTIONAL ABUSE
EXTORTION
FALSIFICATION OF ACCOUNTS
FEMALE GENITAL MUTILATION
FICTITIOUS TRADING
FINANCIAL ABUSE
FORGERY
FRAUD
FRAUDULENT BREACH OF TRUST
GENDER VIOLENCE - RAPE
GENOCIDE
HARASSMENT
HARBOURING
HINDERING BURIAL OF DEAD BODY
HINDERING BURIALS
ILLEGAL IMMIGRATION
IMPORTATION OF EXPLOSIVES
INCEST
INDECENT ASSAULT
INSULTING COURT
ISSUE OF FALSE CHEQUE
KIDNAPPING
MANSLAUGHTER
MOTOR TRAFFIC OFFENCES
MURDER
NARCOTIC DRUG OFFENCES
NEGLIGENTLY CAUSING HARM
NOXIOUS TRADE
OTHER OFFENCES
PERJURY
PHYSICAL ABUSE
PIRACY
POSSESSION OF EXPLOSIVES FIREARMS
POSSESSION OF STOLEN PROPERTY
PROSTITUTION
PUBLIC NUISANCE
PUBLICATION OF FALSE NEWS
PYSCHOLOGICAL/MENTAL ABUSE
RAPE
RESISTING ARREST
RIOT
ROBBERY
RUBBISH IN FRONT OF PREMISES
SEDUCTION
SLAVE DEALING
SMUGGLING
STEALING
STRAY CATTLE
TAX EVASION OFFENCES
THREAT OF DEATH
THREAT OF HARM
THROWING RUBBISH IN STREET
TREASON
TRESPASS
UNLAWFUL DAMAGE
UNLAWFUL ENTRY
UNLAWFUL FIGHTS
UNLAWFUL OATH
UNLAWFUL PUBLICATION
UNLAWFUL TRAINING
UNWHOLESOME FOOD
USE OF OFFENSIVE WEAPON
USING PUBLIC OFFICE FOR PROFIT


class PoliceStation(RootModel):
    region = models.ForeignKey(Region,null=True,blank=True)
    name = models.CharField(max_length=250)
    town =  models.CharField(max_length=200,null=True,blank=True)#models.ForeignKey(Town,null=True,blank=True)
    phone_number = models.CharField(max_length=20,null=True,blank=True)
    email_address = models.EmailField(unique=True, null=True)

    def __str__(self):
        return " %s ( %s )"  %(self.name,self.region)

    class Meta:
        verbose_name  = 'Police'
        verbose_name_plural  = 'Police'




class AppealsCourt(models.Model):
    file_number  = models.CharField(max_length=50, null=False, blank=False)
    name = models.CharField(max_length= 500,null=False)
    suit_number  = models.CharField(max_length=50,null=False,blank=False)
    attorney  = models.ForeignKey(StateAttorney,null=True,blank=True)
    offence = models.ForeignKey(Offence,null=True,blank=True)
    recommendation  = models.CharField(max_length=500, blank=True, null=True)
    date_closed = models.DateField(null=True,blank=True)

    def __str__(self):
        return self.name


def  clean(self):
        for attorney in self.attorneys.all():
            if  not attorney.region == self.region == self.investigator__region:
                raise ValidationError('Investigator and State Attorney must belong to the same region as Docket')





'''

