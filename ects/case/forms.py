from django import forms
from case.models import   Case

from django.core.exceptions import ValidationError
from case.models import  Motion
from case.models import Docket,AssignedDockets
from case.models import Offence,OffenceCategory
from case.models import Station,Investigator
from case.models import Accused,StateAttorney

from dal import  autocomplete
from django import forms


class OffenceForm(forms.ModelForm):
    #ModelMultipleChoiceField
    #ModelChoiceField
        category = forms.ModelMultipleChoiceField(
            queryset = OffenceCategory.objects.all(),
            widget = autocomplete.ModelSelect2Multiple(url='offencecategory-autocomplete')
        )

        class Meta:
            model = Offence
            fields = ('__all__')


class InvestigatorForm(forms.ModelForm):

        station = forms.ModelChoiceField(
           # queryset = Station.objects.all()      #filter(station__region = self.request.user.profile.station),
            queryset = Station.objects.all(),#filter(station__region = self.request.user.profile.station),
            #widget = autocomplete.ModelSelect2Multiple(url='station-autocomplete')
            widget = autocomplete.ModelSelect2(url='station-autocomplete')
        )



class AccusedForm(forms.ModelForm):

    offence = forms.ModelMultipleChoiceField(
            queryset = Offence.objects.all(),
            widget = autocomplete.ModelSelect2Multiple(url='offence-autocomplete')
        )

    class Meta:
        model = Accused
        fields = ('__all__')


class MotionForm(forms.ModelForm):

    def  __init__(self,*args,**kwargs):
        self.request = kwargs.pop('request',None)
        super(MotionForm,self).__init__(*args, **kwargs)

        offence = forms.ModelMultipleChoiceField(
            queryset = Offence.objects.all(),
            widget = autocomplete.ModelSelect2Multiple(url='offence-autocomplete')
        )

        attorneys = forms.ModelMultipleChoiceField(
            queryset = Offence.objects.all(),
            widget = autocomplete.ModelSelect2Multiple(url='attorney-autocomplete')
        )


class DocketForm(forms.ModelForm):

    def  __init__(self,*args,**kwargs):
        self.request = kwargs.pop('request',None)
        super(DocketForm,self).__init__(*args, **kwargs)

        investigator = forms.ModelMultipleChoiceField(
           # queryset = Investigator.objects.all(),#.filter(station__region = self.request__user.profile.station),
            queryset = Investigator.objects.filter(station__region = request.user.profile.station),

            widget = autocomplete.ModelSelect2Multiple(url='investigator-autocomplete')
        )

    class Meta:
        model = Docket
        fields = ('__all__')



class AssignedDocketsInlineForm(forms.ModelForm):

        attorney = forms.ModelChoiceField(
        queryset = StateAttorney.objects.all(),#StateAttorney.objects.filter(station__region = self.request.user.profile.station),

            widget = autocomplete.ModelSelect2(url='attorney-autocomplete')
        )

        class Meta:
            model = AssignedDockets
            fields = ('__all__')




class AssignedMotionsInlineForm(forms.ModelForm):

        attorney = forms.ModelChoiceField(
        queryset = StateAttorney.objects.all(),#StateAttorney.objects.filter(station__region = self.request.user.profile.station),

            widget = autocomplete.ModelSelect2(url='attorney-autocomplete')
        )

        class Meta:
            model = AssignedDockets
            fields = ('__all__')





'''
Single-field Validation

class ToDoForm(forms.Form):
    task = forms.CharField(label=_("Task"))
    done = forms.BooleanField(label=_("Done"))

    #single field validation
    def clean_task(self):
        if self.cleaned_data['task'] == u"nothing":
            raise forms.ValidationError(_("That's just silly"))

#Inter-field validation
Class ToDoForm(forms.Form):
    task = forms.CharField(label=_("Task:"))
    done = forms.BooleanField(label=_("Done:"))

    def clean_done(self):
        # order of fields above matters!
        task = self.cleaned_data['task']
        done = self.cleaned_data['done']
        if task == u"prove 1 = 0" and done:
            raise forms.ValidationError(_("Please re-check your work"))


#Save the User
#base class for forms that store the user from the request object for later use in validation.
class FormRequestUser(forms.Form):
    def __init__(self, request, *args, **varargs):
        self.user = request.user
        super(FormRequestUser, self).__init__(*args, **varargs)


#ModelForm Save the User
#automatically set the user foreign key in the model being saved to the user logged in.
class ModelFormRequestUser(forms.ModelForm):
    def __init__(self, request, *args, **varargs):
        self.user = request.user
        super(ModelFormRequestUser, self).__init__(*args, **varargs)

    def save(self, commit=True):
        obj = super(ModelFormRequestUser, self).save(commit=False)
        obj.user = self.user
        if commit:
            obj.save()
            self.save_m2m() # careful with ModelForms + commit=False
        return obj


#Custom Options for User
class PlaceForm(FormRequestUser):
    place = forms.ChoiceField(label=_("Select your workplace"),
        choices=BLANK_CHOICE_DASH, required=True)

    def __init__(self, request, *args, **varargs):
        super(PlaceForm, self).__init__(request, *args, **varargs)
        if self.user.employer:
            self.set_employer(self.user.employer)

    def set_employer(self, employer):
        place_choices = BLANK_CHOICE_DASH + [
            (place.id, place.name) for place in employer.workplaces]
        self.fields['worksite'].choices = place_choices
Once we know the user we can customize forms for them. This form populates a ChoiceField's choices based on the the user's employer workplaces. The ChoiceField built-in validation will ma



#automatically sets  forms region to region of user logged
class ModelFormRequestUser(forms.ModelForm):
    def __init__(self, request, *args, **varargs):
        self.user = request.user
        super(ModelFormRequestUser, self).__init__(*args, **varargs)



class MotionForm(forms.ModelForm):

    #custom field
    confirm_offence = forms.CharField(
        label='Confirm Offence',
        required = True

        )

    class Meta:
        model  = Motion
        #fields = '__all__'
        #fields = ['title_of_case','hearing_date','suit_no']


    def __init__(self,*args,**kwargs):
        if kwargs.get('instance'):
            offence = kwargs['instance'].offence
            kwargs.setdefault('initial',{})['confirm_email']  = offence
        return super(MotionForm,self).__init__(*args,**kwargs)

    def clean(self):
        if self.cleaned_data.get('Offence') != self.cleaned_data.get('confirm_Offence'):
            raise ValidationError('Offence must match')

        return self.cleaned_data

'''

