

from dal import  autocomplete
from django import forms
from django.contrib.admin import AdminSite
from django.http import HttpResponse
from django.contrib.admin import SimpleListFilter
from django.db.models import Q


from .models import Docket,DocketReceived,DocketMovement,DocketDispatch
from django.contrib import admin
from .models import OffenceCategory,Offence,AccusedType
from .models import Station,StateAttorney
from .models import DocketStatus,MotionStatus,AppealStatus,PetitionStatus,AssignedDockets
from .models import  Region,District, Town

from .models import Advice
from .models import Motion,NatureOfMotion,AssignedMotions
from .models import Petition
from .models import Case
from .models import Station, Investigator,InvestigativeBody
from .models import OffenceCategory,Offence,Station
from .models import AGStaff,StateAttorney
from .models import Appeal,GeneralCorrespondence, PardonsAndExtradiction,Accused
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter


from .forms import OffenceForm,InvestigatorForm,AccusedForm,DocketForm
from .forms import MotionForm,AssignedDocketsInlineForm,AssignedMotionsInlineForm



admin.site.site_header = 'Criminal Case Register'


class  CorruptionOffenceDocketFilters(SimpleListFilter):
    title = ('Docket Offence Categories')
    parameter_name = 'accused'

    def lookups(self,request,model_admin):
        #qs = Offence.Objects.filter(id__in = model_admin.model.objects.all().values_list('offence_id',flat=True).distinct() )
        #qs =  Docket.objects.filter(accused__offence__category__category='Corruption')
        qs  = OffenceCategory.objects.filter( Q(category= 'Corruption')  | Q(category= 'Environmental')
            | Q(category= 'ENVIRONMENTAL OFFENCE')|Q(category= 'CORRUPTION'))
        # corrupt_offence_dockets

        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c.category])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(accused__offence__category__id__exact=self.value())
        else:
            return queryset


class  CorruptionOffenceMotionFilters(SimpleListFilter):
    title = '   Offence Categories'
    parameter_name = 'accused'

    def lookups(self,request,model_admin):
        #qs = Offence.Objects.filter(id__in = model_admin.model.objects.all().values_list('offence_id',flat=True).distinct() )
        #qs =  Docket.objects.filter(accused__offence__category__category='Corruption')
        qs  = OffenceCategory.objects.filter( Q(category= 'Corruption')  | Q(category= 'Environmental')
            | Q(category= 'ENVIRONMENTAL OFFENCE')|Q(category= 'CORRUPTION'))
        # corrupt_offence_dockets

        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c.category])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(offence__category__id__exact=self.value())
        else:
            return



class AttorneyDocketFilter(SimpleListFilter):
    title = 'State Attorney Assigned'
    parameter_name  = "attorneys"

    def lookups(self,request,model_admin):
        qs  = StateAttorney.objects.filter(region=request.user.profile.station)
        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            #return queryset.filter(accused__offence__category__id__exact=self.value())
            return queryset.filter(attorneys__id__exact=self.value())
        else:
            return queryset



class AttorneyAdviceFilter(SimpleListFilter):
    title = 'State Attorney Assigned'
    parameter_name  = "attorney"

    def lookups(self,request,model_admin):
        qs  = StateAttorney.objects.filter(region=request.user.profile.station)
        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            #return queryset.filter(accused__offence__category__id__exact=self.value())
            return queryset.filter(attorney__id__exact=self.value())
        else:
            return queryset

class AttorneyMotionFilter(SimpleListFilter):
    title = 'State Attorney Assigned'
    parameter_name  = "attorneys"

    def lookups(self,request,model_admin):
        #qs = Offence.Objects.filter(id__in = model_admin.model.objects.all().values_list('offence_id',flat=True).distinct() )
        #qs =  Docket.objects.filter(accused__offence__category__category='Corruption')
        #qs  = OffenceCategory.objects.filter( Q(category= 'Corruption')  | Q(category= 'Environmental')
        #    | Q(category= 'ENVIRONMENTAL OFFENCE')|Q(category= 'CORRUPTION'))
        # corrupt_offence_dockets
        qs  = StateAttorney.objects.filter(region=request.user.profile.station)

        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(attorneys__id__exact=self.value())
        else:
            return queryset


class AttorneyPetitionFilter(SimpleListFilter):
    title = 'State Attorney Assigned'
    parameter_name  = "attorney"

    def lookups(self,request,model_admin):
        #qs = Offence.Objects.filter(id__in = model_admin.model.objects.all().values_list('offence_id',flat=True).distinct() )
        #qs =  Docket.objects.filter(accused__offence__category__category='Corruption')
        #qs  = OffenceCategory.objects.filter( Q(category= 'Corruption')  | Q(category= 'Environmental')
        #    | Q(category= 'ENVIRONMENTAL OFFENCE')|Q(category= 'CORRUPTION'))
        # corrupt_offence_dockets
        qs  = StateAttorney.objects.filter(region=request.user.profile.station)

        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(attorney__id__exact=self.value())
        else:
            return queryset



class AttorneyAppealFilter(SimpleListFilter):
    title = 'State Attorney Assigned'
    parameter_name  = "attorney"

    def lookups(self,request,model_admin):
        #qs = Offence.Objects.filter(id__in = model_admin.model.objects.all().values_list('offence_id',flat=True).distinct() )
        #qs =  Docket.objects.filter(accused__offence__category__category='Corruption')
        #qs  = OffenceCategory.objects.filter( Q(category= 'Corruption')  | Q(category= 'Environmental')
        #    | Q(category= 'ENVIRONMENTAL OFFENCE')|Q(category= 'CORRUPTION'))
        # corrupt_offence_dockets
        qs  = StateAttorney.objects.filter(region=request.user.profile.station)

        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(attorney__id__exact=self.value())
        else:
            return queryset




class AttorneyPardonsAndExtradictionlFilter(SimpleListFilter):
    title = 'State Attorney Assigned'
    parameter_name  = "attorney"

    def lookups(self,request,model_admin):
        #qs = Offence.Objects.filter(id__in = model_admin.model.objects.all().values_list('offence_id',flat=True).distinct() )
        #qs =  Docket.objects.filter(accused__offence__category__category='Corruption')
        #qs  = OffenceCategory.objects.filter( Q(category= 'Corruption')  | Q(category= 'Environmental')
        #    | Q(category= 'ENVIRONMENTAL OFFENCE')|Q(category= 'CORRUPTION'))
        # corrupt_offence_dockets
        qs  = StateAttorney.objects.filter(region=request.user.profile.station)

        list_tuple = []
        for c in qs:
             #corrupt_offence_dockets.append([c.id,c.title])
             list_tuple.append([c.id,c])
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(attorney__id__exact=self.value())
        else:
            return queryset


class SuperuserAwareModelAdmin(admin.ModelAdmin):
    superuser_fields = None
    superuser_fieldsets = None

    def get_fieldsets(self, request, obj = None):
        if request.user.is_superuser and self.superuser_fieldsets:
            return (self.fieldsets or tuple()) + self.superuser_fieldsets
        return super(SuperuserAwareModelAdmin, self).get_fieldsets(request, obj)

    def get_fields(self, request, obj = None):
        if request.user.is_superuser and self.superuser_fields:
            return (self.fields or tuple()) + self.superuser_fields
        return super(SuperuserAwareModelAdmin, self).get_fields(request, obj)


class SuperUserAwareAdmin(admin.ModelAdmin):
    normaluser_fields = ['field1','field2']
    superuser_fields = ['special_field1','special_field2']

    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            self.fields = self.normaluser_fields + self.superuser_fields
        else:
            self.fields = self.normaluser_fields

        #return super(MyAdmin, self).get_form(request, obj, **kwargs)
        return super(MyAdmin, self).get_form(request, obj, **kwargs)

#adds regional filters to save and list display
class RegionalBaseModelAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'region':
            if  not request.user.is_superuser:
                kwargs['initial'] = request.user.profile.station
        return super(RegionalBaseModelAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )


    def get_queryset(self,request):
           # return qs.filter(region__name=request.user.profile.station__name)
            qs  = super(RegionalBaseModelAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs#.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)

            #add regional filter to queryset to restrict views to onl


    def get_fields(self, request, obj = None):
        fields  = super(RegionalBaseModelAdmin, self).get_fields(request, obj)
        if request.user.is_superuser:
            return fields # and self.superuser_fields:
        return   tuple( field for field in fields  if field != 'region' )


    def save_model(self, request, obj, form, change):
        #instance = form.save(commit=False)
       # instance.region = request.user.profile.station
        #instance.save(request=request)
       # return instance
       obj.region = request.user.profile.station
       super(RegionalBaseModelAdmin, self).save_model(request, obj, form, change)


#Filters the foreignkey options by region of logged_in user
class RegionBasedForeignkey(admin.ModelAdmin):


    #filters choices for all foreignkey fields on the admin site:
    def formfield_for_foreignkey(self,request,**kwargs):
        if request.user.is_superuser:
            return super(RegionBasedForeignkey, self).formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            if hasattr(db_field.rel.to, 'region'):
                kwargs['queryset'] = db_field.rel.to.objects.filter(region= request.user.profile.station)
            if hasttr(db_field.rel.to,'user'):
                kwargs['queryset'] =db_field.rel.to.objects.filter()
            return super(ModelAdminFront, self).formfield_for_foreignkey(db_field, request, **kwargs)


    def get_queryset(self,request):
           # return qs.filter(region__name=request.user.profile.station__name)
            qs  = super(RegionBasedForeignkey,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs#.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)

            #add regional filter to queryset to restrict views to onl


class RegionAdmin(admin.ModelAdmin):
   # change_list_template = 'admin/region_change_list.html'
    date_hierachy  =  'created'
    search_fields = ( 'name','code')
    #value must be a foreignkey or a many to many field
    list_display = ('name', 'code')
    list_filter = ('code',)
    ordering = ['name']



class StationAdmin(RegionalBaseModelAdmin):

    list_display = ('name','region')
    search_fields  = ('name','region',)
    ordering = ['name']
   # raw_id_fields  = ('region' ,)


class InvestigativeBodyAdmin(admin.ModelAdmin):
    date_hierachy  =  'created'
    search_fields = ( 'name',)
    ordering = ('name',)
    list_display  = ('code','name')



class InvestigatorAdmin(admin.ModelAdmin):

    search_fields = ( 'lastname','badge_number')
    list_display = ('title','firstname','lastname','phone_number','email' ,'station')
    #ist_filter = ('stations',)
    ordering = ('lastname',)

    def get_form(self, request, obj=None,*args, **kwargs):
        form = super(InvestigatorAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['station'] = forms.ModelChoiceField(
               #queryset = Investigator.objects.all(),#.filter(station__region = self.request__user.profile.station),
                queryset = Station.objects.all(),#.filter(region = request.user.profile.station),
                widget = autocomplete.ModelSelect2(url='station-autocomplete')
            )
        return form


    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(InvestigatorAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs.filter(station__region= request.user.profile.station)
            return qs.filter(station__region=request.user.profile.station)



    def get_fields(self, request, obj = None):
                fields  = super(InvestigatorAdmin, self).get_fields(request, obj)
                if request.user.is_superuser:
                    return fields # and self.superuser_fields:
                return   tuple( field for field in fields  if field != 'region' )




class OffenceCategoryAdmin(admin.ModelAdmin):
    search_fields = ( 'category',)
    list_display = ('category',)
    ordering = ['category']


class OffenceAdmin(admin.ModelAdmin):
    form = OffenceForm
    search_fields = ( 'offence' ,'category')
    list_display = ('offence',)
    ordering = ['category']


class AccusedTypeAdmin(admin.ModelAdmin):
    search_fields = ( 'type', )
    list_display = ('type',)
    ordering = ['type']



class NatureOfMotionAdmin(admin.ModelAdmin):
    search_fields = ( 'nature', )
    list_display = ('nature',)
    ordering = ['nature']



class AccusedInline(admin.TabularInline):
    form = AccusedForm
    model =  Accused
    extra =1
    verbose_name = "Accused"
    verbose_name_plural = "Accused Person(s)"

    classes = ['collapse']



class InvestigatorInline(RegionalBaseModelAdmin):
    form = InvestigatorForm
    model =  Investigator
    extra =1
    verbose_name = "Investigator"
    verbose_name_plural = "Investigator"

    classes = ['collapse']


class AGStaffAdmin(RegionalBaseModelAdmin):
   # readonly_fields = ['region']
    exclude = ['region']
    search_fields = ( 'firstname','lastname')
    list_display = ('firstname','lastname','phone_number','region',"unit",'status')
    ordering = ['lastname']
    #raw_id_fields=('region',)
    list_filter = ('status','unit','region')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'region':
            kwargs['initial'] = request.user.profile.station
        return super(AGStaffAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

    def get_form(self, request, *args, **kwargs):
         form = super(AGStaffAdmin, self).get_form(request, *args, **kwargs)
         #form.firstname = request.user.profile.station
         form.firstname = request.user.profile.staff_id
         return form

    def save_model(self, request, obj, form, change):
        #instance = form.save(commit=False)
       # instance.region = request.user.profile.station
        #instance.save(request=request)
       # return instance
       obj.region = request.user.profile.station
       super(AGStaffAdmin, self).save_model(request, obj, form, change)



class DocketStatusAdmin(admin.ModelAdmin):
        list_display=('status',)
        verbose_name = "Docket Status"
        verbose_name_plural = "Docket Status"



class MotionStatusAdmin(admin.ModelAdmin):
        list_display=('status',)
        verbose_name = "Motion Status"
        verbose_name_plural = "Motion Status"

class AppealStatusAdmin(admin.ModelAdmin):
        list_display=('status',)
        verbose_name = "Appeal Status"
        verbose_name_plural = "Appeal Status"


class PetitionStatusAdmin(admin.ModelAdmin):
        list_display=('status',)
        verbose_name = "Appeal Status"
        verbose_name_plural = "Appeal Status"


#needs a reginon filter
#RegionBasedForeignkey
#class DocketReceivedAdmin(admin.ModelAdmin):
class DocketReceivedAdmin(RegionBasedForeignkey):

        list_display  = ('title','registered_offense_number','received_by','region','received_from','date_received',)
        #raw_id_fields=('region',)
        search_fields  =  ['title','registered_offense_number']
        #list_filter = ('received_by')



#needs a region filter
class RootAdminWithRequest(admin.ModelAdmin):
        form  =  DocketForm # RootAdminWithRequestForm

        def get_form(self,request,obj=None,**kwargs):
            AdminForm = super(RootAdminWithRequest,self).get_form(request,obj,**kwargs)

            class AdminFormWithRequest(AdminForm):
                def  __new__(cls,*args,**kwargs):
                    kwargs['request']  = request
                    return AdminForm(*args,**kwargs)
            return AdminFormWithRequest




class AssignedDocketsInline(admin.TabularInline):
    model =  AssignedDockets
    extra = 1
    form = AssignedDocketsInlineForm


    def get_form(self,request,obj=None,**kwargs):
            form = super(AssignedDocketsInline, self).get_form(request, obj, **kwargs)

            form.base_fields['attorney']   = forms.ModelChoiceField(
                queryset =  StateAttorney.objects.filter(region = request.user.profile.station),
                widget = autocomplete.ModelSelect2(url='attorney-autocomplete')
            )

            return form


        #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(AssignedDocketsInline,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs#.filter(region=request.user.profile.station)





class StateAttorneyAdmin(RegionalBaseModelAdmin):
    list_display  = ('firstname','lastname','phone_number','email_address','staff_id','region')
    search_fields = ('lastname','email_address')
    inlines = [AssignedDocketsInline]


#needs a region filter RegionalBaseModelAdmin
class DocketAdmin(RegionalBaseModelAdmin):
#class DocketAdmin(RootAdminWithRequest):

        list_display  = ('title','registered_offense_number','region','docket_number','status',)
        search_fields  =  ['title','registered_offense_number']
        list_filter = (CorruptionOffenceDocketFilters,AttorneyDocketFilter,
                    ('date_of_submission', DateRangeFilter), ('date_of_charge_sheet', DateTimeRangeFilter),
        )
        inlines = (AccusedInline,AssignedDocketsInline)

        actions = ['export_to_pdf']


        def get_form(self,request,obj=None,**kwargs):
            form = super(DocketAdmin, self).get_form(request, obj, **kwargs)
            form.base_fields['investigator'] = forms.ModelMultipleChoiceField(
               #queryset = Investigator.objects.all(),#.filter(station__region = self.request__user.profile.station),
                queryset = Investigator.objects.filter(station__region = request.user.profile.station),
                widget = autocomplete.ModelSelect2Multiple(url='investigator-autocomplete')
            )
            return form


        def export_to_pdf(self,request, queryset):
            from reportlab.lib import  colors
            from reportlab.lib.pagesizes import A4,inch ,landscape
            from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
            from reportlab.lib.styles import getSampleStyleSheet

            doc = SimpleDocTemplate()


        #add regional context filter
        def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(DocketAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(region = request.user.profile.station  )




#class DocketMovementAdmin(RegionalBaseModelAdmin):
class DocketMovementAdmin(admin.ModelAdmin):

    list_display  = ('docket','from_whom','to_whom','remarks')
    search_fields  =  ['docket__title','from_whom__lastname','to_whom__lastname','from_whom__firstname']
    list_filter = ('from_whom','to_whom', ('date', DateRangeFilter),)

    def get_form(self,request,obj=None,**kwargs):
            form = super(DocketMovementAdmin, self).get_form(request, obj, **kwargs)
            form.base_fields['docket'] = forms.ModelMultipleChoiceField(
               #queryset = Investigator.objects.all(),#.filter(station__region = self.request__user.profile.station),
                queryset = Docket.objects.filter(region = request.user.profile.station),
                widget = autocomplete.ModelSelect2Multiple(url='docket-autocomplete')
            )
            return form


        #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(DocketMovementAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(docket__region = request.user.profile.station  )


class DocketDispatchAdmin(RegionalBaseModelAdmin):
    list_display  = ('docket','date_of_dispatch','subject','registry_no','date_of_letter','date_of_dispatch','officer')
    search_fields  =  ['docket__title','registry_no','subject']
    list_filter = ( ('date_of_dispatch', DateRangeFilter), ('date_of_letter', DateRangeFilter))

    def get_form(self,request,obj=None,**kwargs):
        form = super(DocketDispatchAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['docket']   = forms.ModelChoiceField(
           queryset = Docket.objects.filter(region = request.user.profile.station),
            widget = autocomplete.ModelSelect2(url='docket-autocomplete')
        )
        return form


#needs a region filter
class AssignedDocketsAdmin(admin.ModelAdmin):
    model = AssignedDockets
    list_display  = ('attorney','docket','date_assigned','date_received','remarks')
    search_fields  =  ['docket','attorney']
    #list_filter = ('received_by')
    #list_filter = ('attorney',)




#needs a region filter
class PetitionAdmin(admin.ModelAdmin):
    list_display  = ('file_number','title','attorney','status')
    search_fields  =  ['title','attorney__lastname','attorney__firstname']
    list_filter = (AttorneyPetitionFilter, )
     #('date_received', DateRangeFilter),        ('date_closed', DateRangeFilter))

    def get_form(self,request,obj=None,**kwargs):
        form = super(PetitionAdmin, self).get_form(request, obj, **kwargs)

        form.base_fields['attorney'] = forms.ModelChoiceField(
                queryset = StateAttorney.objects.filter(region = request.user.profile.station),
                widget = autocomplete.ModelSelect2(url='attorney-autocomplete')
             )

        return form

    #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(PetitionAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)


#needs a region filter
#class AdviceAdmin(admin.ModelAdmin):
class AdviceAdmin(RegionalBaseModelAdmin):

    list_display  = ('docket','action_taken','dispatch_date','region',)
    search_fields  = ('docket__title','dispatch_date')
    list_filter = (AttorneyAdviceFilter,('dispatch_date', DateRangeFilter),)

    def get_form(self,request,obj=None,**kwargs):
        form = super(AdviceAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['docket']   = forms.ModelChoiceField(
           queryset = Docket.objects.filter(region = request.user.profile.station),
            widget = autocomplete.ModelSelect2(url='docket-autocomplete')
        )

        #attorney m2m
        form.base_fields['attorney'] = forms.ModelMultipleChoiceField(
               #queryset = Investigator.objects.all(),#.filter(station__region = self.request__user.profile.station),
            queryset = StateAttorney.objects.filter(region = request.user.profile.station),
            widget = autocomplete.ModelSelect2Multiple(url='attorney-autocomplete')
        )
        form.region = request.user.profile.station
        return form


        #add regional context filter
    def get_queryset(self,request):
            qs  = super(AdviceAdmin,self).get_queryset(request)
            return qs.filter(region=request.user.profile.station)

            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(regio=request.user.profile.station)



#motions

class AssignedMotionsInline(admin.TabularInline):
    model =  AssignedMotions
    extra = 1
    form = AssignedMotionsInlineForm

    verbose_name = "Atttorney"
    verbose_name_plural = "Attorneys Assigned To"


    def get_form(self,request,obj=None,**kwargs):
            form = super(AssignedMotionsInline, self).get_form(request, obj, **kwargs)

            form.base_fields['attorney']   = forms.ModelChoiceField(
                queryset =  StateAttorney.objects.filter(region = request.user.profile.station),
                widget = autocomplete.ModelSelect2(url='attorney-autocomplete')
            )

            return form

        #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(AssignedMotionsInline,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(attorney__region=request.user.profile.station)



class MotionAdmin(RegionalBaseModelAdmin):
    list_display  = ('title_of_case','file_number','nature','suit_no', 'status','region','date_of_service')
    search_fields  =   ['title_of_case','suit_no','offence__offence']
    list_filter  = (CorruptionOffenceMotionFilters, AttorneyMotionFilter,'status',
         ('date_of_service', DateRangeFilter),)
    inlines = [AssignedMotionsInline]


    def get_form(self,request,obj=None,**kwargs):
            form = super(MotionAdmin, self).get_form(request, obj, **kwargs)
            form.base_fields['offence'] = forms.ModelMultipleChoiceField(
                 queryset = Offence.objects.all(),
                 widget = autocomplete.ModelSelect2Multiple(url='offence-autocomplete')
            )
            return form

    #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(MotionAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)



#Appeals
class AppealAdmin(admin.ModelAdmin):
    list_display  = ('title_of_case','suit_number', 'file_number','offence','status','date_received','date_assigned')
    search_fields  =  ['title_of_case','suit_number', 'file_number','offence__offence','status__status']
    #list_filter = ('received_by')
    list_filter = (AttorneyAppealFilter, ('date_received', DateRangeFilter), ('date_assigned', DateRangeFilter),
         ('date_closed', DateRangeFilter),)


    def get_form(self,request,obj=None,**kwargs):
        form = super(AppealAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['attorney']   = forms.ModelChoiceField(
           queryset = StateAttorney.objects.filter(region = request.user.profile.station),
            widget = autocomplete.ModelSelect2(url='attorney-autocomplete')
        )
        return form


    #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(AppealAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)






class GeneralCorrespondenceAdmin(RegionalBaseModelAdmin):
    list_display  = ('file_number','date_received','date_on_letter', 'reference','officer','comments')
    #raw_id_fields=('region',)
    search_fields  =  ['subject',]
    list_filter = (  ('date_received', DateRangeFilter),('date_on_letter', DateRangeFilter),
        ('date_dispatched', DateRangeFilter),
        )

     #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(GeneralCorrespondenceAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)




class PardonsAndExtradictionsAdmin(admin.ModelAdmin):
    list_display  = ('file_number', 'title_of_case','date_received','date_assigned','date_assigned','remarks')
    search_fields  = ('title_of_case','date_assigned')
    list_filter  = (AttorneyPardonsAndExtradictionlFilter,  ('date_received', DateRangeFilter),
        ('date_assigned', DateRangeFilter),
         ('date_dispatched', DateRangeFilter),
        )

    def get_form(self,request,obj=None,**kwargs):
        form = super(PardonsAndExtradictionsAdmin, self).get_form(request, obj, **kwargs)

        form.base_fields['attorney'] = forms.ModelMultipleChoiceField(
                queryset = StateAttorney.objects.filter(region = request.user.profile.station),
                widget = autocomplete.ModelSelect2Multiple(url='attorney-autocomplete')
             )

        return form

    #add regional context filter
    def get_queryset(self,request):
           # return qs.filter(region=request.user.profile.station)
            qs  = super(PardonsAndExtradictionsAdmin,self).get_queryset(request)
            if request.user.is_superuser:
            #superuser sees all
                return qs #.filter(region= request.user.profile.station)
            return qs.filter(region=request.user.profile.station)



admin.site.register(Region, RegionAdmin)
admin.site.register(InvestigativeBody, InvestigativeBodyAdmin)
admin.site.register(Station, StationAdmin)

admin.site.register(Investigator,InvestigatorAdmin)
admin.site.register(OffenceCategory,OffenceCategoryAdmin)
admin.site.register(Offence,OffenceAdmin)
admin.site.register(AccusedType,AccusedTypeAdmin)

admin.site.register(AGStaff,AGStaffAdmin)

admin.site.register(DocketStatus, DocketStatusAdmin)
admin.site.register(MotionStatus, MotionStatusAdmin)
admin.site.register(AppealStatus, AppealStatusAdmin)
admin.site.register(PetitionStatus, PetitionStatusAdmin)
admin.site.register(StateAttorney,StateAttorneyAdmin)

admin.site.register(NatureOfMotion, NatureOfMotionAdmin)



#received
#admin.site.register(DocketReceived,DocketReceivedAdmin)
#filed
admin.site.register(Docket, DocketAdmin)


#sendto
admin.site.register(DocketMovement,DocketMovementAdmin)
admin.site.register(DocketDispatch, DocketDispatchAdmin)


admin.site.register(Advice, AdviceAdmin)

admin.site.register(Motion,MotionAdmin)

admin.site.register(Appeal,AppealAdmin)

admin.site.register(Petition,PetitionAdmin)

admin.site.register(GeneralCorrespondence,GeneralCorrespondenceAdmin)
admin.site.register(PardonsAndExtradiction,PardonsAndExtradictionsAdmin)
