# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-16 06:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0007_auto_20171116_0618'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pardonsandextradiction',
            name='attorney',
            field=models.ManyToManyField(blank=True, to='case.StateAttorney'),
        ),
    ]
