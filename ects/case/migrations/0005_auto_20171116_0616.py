# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-16 06:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0004_auto_20171116_0456'),
    ]

    operations = [
        migrations.AddField(
            model_name='motion',
            name='nature',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='case.NatureOfMotion'),
        ),
        migrations.AddField(
            model_name='motion',
            name='status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='case.MotionStatus'),
        ),
        migrations.AlterField(
            model_name='assignedmotions',
            name='remarks',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='generalcorrespondence',
            name='comments',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.RemoveField(
            model_name='pardonsandextradiction',
            name='attorney',
        ),
        migrations.AddField(
            model_name='pardonsandextradiction',
            name='attorney',
            field=models.ManyToManyField(blank=True, null=True, to='case.StateAttorney'),
        ),
        migrations.AlterField(
            model_name='pardonsandextradiction',
            name='title_of_case',
            field=models.CharField(max_length=500),
        ),
    ]
