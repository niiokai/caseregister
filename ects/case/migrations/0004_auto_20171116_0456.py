# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-16 04:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0003_auto_20171116_0417'),
    ]

    operations = [
        migrations.CreateModel(
            name='NatureOfMotion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now_add=True)),
                ('nature', models.CharField(max_length=500)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='motion',
            name='hearing_date',
        ),
        migrations.RemoveField(
            model_name='motion',
            name='legacy_id',
        ),
        migrations.AddField(
            model_name='motion',
            name='date_of_service',
            field=models.DateTimeField(blank=True, null=True, verbose_name=b'Date of Service'),
        ),
        migrations.AddField(
            model_name='motion',
            name='offence',
            field=models.ManyToManyField(blank=True, null=True, to='case.Offence'),
        ),
        migrations.AlterField(
            model_name='motion',
            name='suit_no',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
