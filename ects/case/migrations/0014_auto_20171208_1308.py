# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-08 13:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0013_auto_20171126_0445'),
    ]

    operations = [
        migrations.CreateModel(
            name='NationaIdType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now_add=True)),
                ('type', models.CharField(max_length=200)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='assigneddockets',
            options={'verbose_name': 'Attorneys Assigned', 'verbose_name_plural': 'Attorneys Assigned'},
        ),
        migrations.AlterField(
            model_name='pardonsandextradiction',
            name='attorney',
            field=models.ManyToManyField(blank=True, null=True, to='case.StateAttorney'),
        ),
    ]
