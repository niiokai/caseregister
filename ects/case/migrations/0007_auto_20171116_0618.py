# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-16 06:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0006_auto_20171116_0617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docket',
            name='investigator',
            field=models.ManyToManyField(blank=True, to='case.Investigator'),
        ),
    ]
