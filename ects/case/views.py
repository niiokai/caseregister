from django.core.urlresolvers import  reverse
from django.views.generic import  ListView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from case.models import Motion
import forms
from django.db.models import Q

from dal import  autocomplete
from .models import Offence, OffenceCategory,Station,Investigator,Docket
from .models import StateAttorney

#printing
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse,HttpResponseNotFound

#reportlab
from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import  HttpResponse


#serve an existing pdf
def pdf_view(request):
    fs = FileSystemStorage()
    filename = 'testpdf'
    if fs.exists(filename):
        with fs.open(filename) as pdfptr:
            response = HttpResponse(pdfptr,content_type='application/pdf')
            #for download prompt
            response['Content-Disposition'] = 'attachement;filename="mypdf.pdf"'
            #for inline display inside browser
            #response['Content-Disposition'] = 'inline; filename="mypdf.pdf"'

            return response
    else:
        return HttpResponseNotFound('The requested pdf was not found in our server')


#example pdf
def hello_pdf(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline;filename="mypdf.pdf'

    buffer =  BytesIO()
    p = canvas.canvas(buffer)

    #start writing pdf
    p.drawString(100,100,'hello world')

    #End writing pdf
    p.showPage()
    p.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response


#SimpleDocTemplate
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch

def test1pdfview(request,):
#def write_pdf_simpledoctemplate_view(request,):

    doc = SimpleDocTemplate("/tmp/somefile.pdf")
    styles = getSampleStyleSheet()
    Story = [Spacer(1,0.2*inch)]
    style = styles['Normal']

    for i in range(100):
        bogustext = ("This is Paragraph number %s.  " % i) * 20
        p = Paragraph(bogustext,style)
        Story.append(p)
        Story.append(Spacer(1,0.2*inch))
    doc.build(Story)

    fs = FileSystemStorage("/tmp")
    with fs.open("somefile.pdf") as pdf:
        response = HttpResponse(pdf,content_type='application/pdf')
        response['Content-Dispostion'  ] = 'attachment;filename="some24hrsgig.pdf"'
        return response
    return response


def printdocketview(request,docket_id):
    d = docket.get(id=dokect)
    doc = SimpleDocTemplate('/tmp/docket.pdf')
    styles = getSampleStyleSheet()
    Story = [Spacer(1,0.2*inch)]
    style = styles('Normal')
    p = Paragraph



class OffenceCategoryAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = OffenceCategory.objects.all()
        if self.q:
            qs = qs.filter(category__istartswith=self.q)
        return qs

class OffenceAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Offence.objects.all()
        if self.q:
            #qs = qs.filter(Q(offence__istartswith=self.q ) | Q(offence_icontains=q) )
            qs = qs.filter(offence__istartswith=self.q  )


        return qs


class StationAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        #qs = Station.objects.allss
        qs = Station.objects.filter(region = self.request.user.profile.station)
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class InvestigatorAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Investigator.objects.filter(station__region = self.request.user.profile.station)
        #qs = Investigator.objects.all() ,  #filter(station__region = self.request.user.profile.region)
        if self.q:
            qs = qs.filter(lastname__istartswith=self.q)
        return qs


class AttorneyAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = StateAttorney.objects.filter(region = self.request.user.profile.station)
        if self.q:
            qs = qs.filter( Q(lastname__istartswith=self.q) | Q(firstname__istartswith=self.q) )
        return qs


class DocketAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Docket.objects.filter(region = self.request.user.profile.station)
        if self.q:
            #qs = qs.filter( Q(title__istartswith=self.q) | Q(registered_offence_number__istartswith=self.q) )

            qs = qs.filter(title__istartswith=self.q )
             #qs.filter(registered_offence_number__istartswith=qs)

        return qs

'''
class MyFormView(View):
    form_class = MyForm
    initial = {'key': 'value'}
    template_name = 'form_template.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>

            return HttpResponseRedirect('/success/')

        return render(request, self.template_name, {'form': form})


class ListMotionView(ListView):
    model = Motion
    template_name  = 'motion_list.html'


class CreateMotionView(CreateView):
    model = Motion
    template_name = 'edit_motion.html'
    form_class =  forms.MotionForm

   # fields = ['legacy_id','title_of_case','hearing_date','suit_no','offence','attourney_assigned']

    def get_success_url(self):
        return reverse('motions-list')

    def get_context_data(self,**kwargs):
        context = super(CreateMotionView,self).get_context_data(**kwargs)
        context['action']  = reverse('motions-new')
        return context


class UpdateMotionView(UpdateView):
    model = Motion
    template_name  = 'edit_motion.html'
    form_class =  forms.MotionForm


    def get_success_url(self):
        return reverse('motions-list')

    def get_context_data(self,**kwargs):
        context = super(UpdateMotionView,self).get_context_data(**kwargs)
        context['action'] =  reverse('motions-edit',kwargs={'pk': self.get_object().id })
        return context

class DeleteMotionView(DeleteView):
    model = Motion
    template_name  = 'delete_motion.html'

    def get_success_url(self):
        return reverse('motions-list')


class DetailMotionView(DetailView):
    model = Motion
    template_name  =  'motion.html'

'''
