from  .serializers import RegionSerializer,APITokenSerializer
from case.models import Region

#from rest_framework import viewsets
#from django.http import  HttpResponse , JsonResponse
#from django.views.decorators.csrf import csrf_exempt

#from rest_framework.renderers import JSONRenderer
#from rest_framework.parsers import JSONParser

from rest_framework import status
from rest_framework.decorators  import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404

from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.authtoken.models import Token
#from rest_framework.authtoken.vi import views


from rest_framework import renderers
from rest_framework import parsers

#Serializers  + CBViews
from rest_framework.authtoken import views as token_views
obtain_auth_token = token_views.obtain_auth_token


#api  root
@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'regions': reverse('region-list', request=request, format=format)
    })

class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (
        parsers.FormParser,
        parsers.MultiPartParser,
        parsers.JSONParser

        )
    renderer_classes =  (renderers.JSONRenderer)

    def post(self,request):
        serializer =  APITokenSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        #get api token or create it
        token, created = Token.objects.get_or_create(user=user)
        api_key =  {
          'api_key' : unicode(token.key)
        }
        return Response(api_key)



class RegionList(APIView):

    def get(self,request,format=None):
        regions = Region.objects.all()
        serializer = RegionSerializer(regions,many=True)
        return Response(serializer.data)

    def post(self,request,format=None):
        serializer  = RegionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RegionDetail(APIView):

    def get_object(self,pk):
        try:
            region = Region.objects.get(pk=pk)
        except Region.DoesNotExist:
            raise HTTP404

    def get(self,request,pk,format=None):
        region = self.get_object(pk)
        serializer  = RegionSerializer(region)
        return Response(serializer.data)

    def put(self,request,pk,format=None):
        region = self.get_object(pk)
        serializer = RegionSerializer(region,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)


#serializers + view mixin classes
from rest_framework import mixins
from rest_framework import generics

class RegionList2(mixins.ListModelMixin,
                               mixins.CreateModelMixin,
                               generics.GenericAPIView):

    authentication_classes = (SessionAuthentication,TokenAuthentication, BasicAuthentication)

    queryset  = Region.objects.all()
    serializer_class  = RegionSerializer

    def get(self,request , *args,**kwargs):
        return self.list(request,*args,**kwargs)

    def post(self,request,*args,**kwargs):
        return self.create(request,*args,**kwargs)

class RegionDetail2(mixins.RetrieveModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.DestroyModelMixin,
                                   generics.GenericAPIView):

    queryset  = Region.objects.all()
    serializer_class = RegionSerializer

    def get(self,request,*args,**kwargs):
        return self.retrieve(request,*args,**kwargs)

    def post(self,request,*args,**kwargs):
        return self.create(request,*args,**kwargs)

    def put(self,request,*args,**kwargs):
        return self.create(request,*args,**kwargs)


#serializers with generic mixins classes

class RegionList3(generics.ListCreateAPIView):
    queryset  = Region.objects.all()
    serializer_class = RegionSerializer

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class RegionDetail3(generics.RetrieveUpdateDestroyAPIView):
    queryset = Region.objects.all()
    serializer_class  = RegionSerializer

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)




'''
@csrf_exempt
@api_view(['GET','POST'])
def region_list(request):
    if request.method == "GET":
        regions = Region.objects.all()
        serializer = RegionSerializer(regions,many=True)
        return Response(serializer.data)

    elif request.method  == 'POST':
        serializer  = RegionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return  Response(serializer,status=status.HTTP_201_CREATED)
        return Response(serializers.erros,status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','PUT','DELETE'])
def region_detail(request,pk,format=None):
    try:
        region = Region.objects.get(pk=pk)
    except  Region.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
            serializer  = RegionSerializer(region)
            return Response(serializer.data)

    elif  request.method == 'PUT':
        data  = request.data
        serializer = RegionSerializer(region,data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
            region.delete()
            return  Response(status = status.HTTP_204_NO_CONTENT)
'''



'''
@csrf_exempt
def region_list(request):
    if request.method == 'GET':
        regions = Region.objects.all()
        serializer  = RegionSerializer(regions, many=True)
        return JsonResponse(serializer.data,safe=False)

    elif  request.method == 'POST':
        data = JSONParser().parse(request)
        serializer  = RegionSerializer(data=data)
        if serializer.is_valid:
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors,status=400)

def region_detail(request,pk):
    #retrieve single objet and return instance
    try:
        region = Region.objects.get(pk=pk)
    except Region.DoesNotExist:
        return  HttpResponse(status=404)

    if request.method == 'GET':
        serializer  = RegionSerializer(region)
        return JsonResponse(serializer.data)

    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = RegionSerializer(serializer,data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializers.errors,status=400 )

    elif request.method == 'DELETE':
        region.delete()
        return HttpResponse(status=200)

class RegionViewSet(viewsets.ModelViewSet):

    queryset = Region.objects.all()
    serializer_class = RegionSerializer
'''


