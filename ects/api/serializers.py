from rest_framework import serializers
from case.models import *
from django.contrib.auth.models import User, Group
import re

def validateEmail(email):
    if len(email) > 6:
        if re.match('\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b', email) != None:
            return 1
    return 0

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class APITokenSerializer(serializers.Serializer):
    email_or_username = serializers.CharField(),
    password  = serializers.CharField()

    def validate(self,attrs):
        email_or_username  = attrs.get('email_or_password')
        password  = attrs.get(password)

        if email_or_password and password:
            #check if user sent email
            if validateEmail(email_or_password):
                user_request = get_object_or_404( User,email=email_or_username)
                email_or_username  = user_request.username

            user = authenticate( username = email_or_password,password=password)

            if user:
                if not user.is_active:
                    msg = 'User account is disabled'
                    raise exceptions.ValidationError(msg)
                else:
                    msg = 'unable to login with provide credentials'
            else:
                msg = 'Include username  or email and password'
                raise exceptions.ValidationError(msg)

            attrs['user']  = user
            return attrs


#class
class RegionSerializer(serializers.Serializer):
    '''class Meta:
        model = Region
        fields = ('codes','name')
    '''
    id  = serializers.IntegerField(read_only=True)
    code = serializers.CharField(required=True,allow_blank=False, max_length=10)
    name = serializers.CharField(required=True,allow_blank=False,max_length=250)

    def create(self,validated_data):
        #create and return given region instance given validated data
        return Region.objects.create(**validated_data)


    def update(self,instance,validated_data):
        #update and return an existing instance given the validated_data
        instance.name = validated_data.get('name',instance.name)
        instance.code  = validated_data.get('code',instance.code)
        instance.save()
        return instance




class CaseSerialzer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)


