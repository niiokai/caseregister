from django.conf.urls import include
from rest_framework import routers
from  case.models import Region
from  api import views
from django.conf.urls import url,include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import RegionList, RegionList2
from .views import RegionDetail, RegionDetail2,obtain_auth_token


from rest_framework.authtoken import views as authtoken_views

router = routers.DefaultRouter()
#srouter.register('regions',views.RegionViewSet)


#obtain
urlpatterns = (

             url(r'^regions/$',views.RegionList2.as_view()),
             url(r'^regions/(?P<pk>[0-9])+/$',views.RegionDetail.as_view()),

           # url(r'^regions/$',views.region_list),
            #url(r'^regions/(?P<pk>[0-9])+/$',views.region_detail),
            # url('^',include(router.urls)),
           # url(r'^api/', include('rest_framework.urls',namespace =' rest_framework'))
    )
urlpatterns += (
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/api-key-auth/', obtain_auth_token),
    url(r'^api-key-auth/', obtain_auth_token),

)
urlpatterns = format_suffix_patterns(urlpatterns)

