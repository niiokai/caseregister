"""
This file was generated with the custommenu management command, it contains
the classes for the admin menu, you can customize this class as you want.

To activate your custom menu add the following to your settings.py::
    ADMIN_TOOLS_MENU = 'ects.menu.CustomMenu'
"""

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu


class CustomMenu(Menu):
    """
    Custom Menu for ects admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)


        self.children += [
           # items.MenuItem(_('Dashboard'), reverse('admin:index')),
           #items.ModelList('Configure'),

            items.MenuItem(_('Home'), reverse('admin:index')),

            items.MenuItem("Dockets",
                    children = [
                       # items.MenuItem('Receive','/admin/case/docketreceived'),
                        items.MenuItem('File','/admin/case/docket'),
                        items.MenuItem('Movements','/admin/case/docketmovement'),
                        items.MenuItem('Dispatch','/admin/case/docketdispatch'),


                    ]
                ),

            items.MenuItem("Advices", '/admin/case/advice'),
            items.MenuItem("Motions", '/admin/case/motion' ),
            items.MenuItem("Appeals", '/admin/case/appeal' ),
            items.MenuItem("Petitions", '/admin/case/petition' ),
            items.MenuItem("Pardons and Extraditions", '/admin/case/pardonsandextradiction' ),
            items.MenuItem("Correspondences ", '/admin/case/generalcorrespondence' ),


            #items.AppList(
              #  _('Applications'),
                #exclude=('django.contrib.*',)
            #),

            #installed apps and their models except exclude
            #if models is provide use the
           # items.AppList(
             #   _('Manage Users'),
              #  models=("django.contrib.*",)
            #),'
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        administrator_menus = items.MenuItem(  'Administration',
              children=[

                    items.MenuItem('Regions', '/admin/case/region'),
                    items.MenuItem('Investigative Bodies', '/admin/case/investigativebody'),
                    items.MenuItem('Investigative Stations', '/admin/case/station'),
                    items.MenuItem('Investigators', '/admin/case/investigator'),

                    #items.MenuItem('AG Staffs', '/admin/case/agstaff'),

                    items.MenuItem('State Attorneys', '/admin/case/stateattorney'),

                    items.MenuItem('Offence Categories', '/admin/case/offencecategory'),
                    items.MenuItem('Offences', '/admin/case/offence'),

                    items.MenuItem('Docket Status', '/admin/case/docketstatus'),
                    items.MenuItem('Motion Status', '/admin/case/motionstatus'),
                    items.MenuItem('Appeal Status', '/admin/case/appealstatus'),
                    items.MenuItem('Petition Status', '/admin/case/petitionstatus'),
                    items.MenuItem('Accused Categories', '/admin/case/accusedtype'),
                    items.MenuItem('Motion Categories', '/admin/case/natureofmotion'),


                    items.MenuItem('System User Categories', '/admin/auth/group'),
                    items.MenuItem('System Users', '/admin/auth/user'),

                ],

            ),

        executiveofficer_menus = items.MenuItem(  'Administration',
              children=[

                    items.MenuItem('Investigative Bodies', '/admin/case/investigativebody'),
                    items.MenuItem('Investigative Stations', '/admin/case/station'),
                    items.MenuItem('Investigators', '/admin/case/investigator'),


                    #items.MenuItem('AG Staffs', '/admin/case/agstaff'),

                    items.MenuItem('State Attorneys', '/admin/case/stateattorney'),

                    items.MenuItem('Offence Categories', '/admin/case/offencecategory'),
                    items.MenuItem('Offences', '/admin/case/offence'),

                    items.MenuItem('Docket Status', '/admin/case/docketstatus'),
                    items.MenuItem('Motion Status', '/admin/case/motionstatus'),
                    items.MenuItem('Appeal Status', '/admin/case/appealstatus'),
                    items.MenuItem('Petition Status', '/admin/case/petitionstatus'),
                    items.MenuItem('Accused Categories', '/admin/case/accusedtype'),
                    items.MenuItem('Motion Categories', '/admin/case/natureofmotion'),
            ],
         ),


        request  = context['request']
        if  request.user.is_superuser:
            #self.children.insert(0,administrator_menus)
           # self.children =tuple(administrator_menus ,self.children)
           self.children[1:1] = administrator_menus
        else:
            self.children[1:1] = executiveofficer_menus

        return super(CustomMenu, self).init_with_context(context)
