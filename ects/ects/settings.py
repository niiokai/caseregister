import os

#date formats
from django.conf.locale.en import formats as en_formats
en_formats.DATETIME_FORMAT = "d b Y H:i:s"

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APPLICATION_DIR = os.path.dirname(globals()['__file__']) + '/../'


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'nv9yxqle$+86e#smd&s!7cj5$oemkne2xzlxwzwn4c+)x3+2m8'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['0.0.0.0','192.168.1.69','127.0.0.1','188.166.157.186','10.16.0.5s']

# Application definition

INSTALLED_APPS = [
   # 'suit',
  'dal',
    'dal_select2',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    #'django.contrib.sites',
    'rangefilter',
        'adminactions',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
   'rest_framework.authtoken',



    #custom
    'userprofile.apps.UserprofileConfig',
    'case',
    #'blog.apps.BlogConfig',
    'api.apps.ApiConfig',


]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ects.urls'

TEMPLATES = [
    {

        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
                        'admin_tools.template_loaders.Loader',
                        #os.path.join(BASE_DIR,'templates'),

                ]   ,
        #'APP_DIRS': True,# in preference of loaders below to allow for use of django_admin tools
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',


            ],
           'loaders' : (

                'admin_tools.template_loaders.Loader',
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ),
        },
    },
]



WSGI_APPLICATION = 'ects.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {

     'default': {
        'ENGINE': 'django.db.backends.mysql',
        #NAME': 'caseregister' ,
        #get_env_variable('DATABASE_NAME'),
        'NAME': 'criminalregister' ,#get_env_variable('DATABASE_NAME'),

        'USER':'testadmin' ,#get_env_variable('DATABASE_USER'),
        'PASSWORD':'testadmin', #get_env_variable('DATABASE_PASSWORD'),
        'HOST': '127.0.0.1',
        'PORT': '',
    },



}


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us  '

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/


PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))


STATIC_URL = '/static/'
STATICFILES_DIRS =  (
    os.path.join(
        os.path .dirname(__file__),
        '../static'
    ),
   # django.contrib.staticfiles.finders. AppDirectoriesFinder'
)
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

SITE_ID =1

#Admin Tools;
ADMIN_TOOLS_MENU = 'menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD =   'dashboard.CustomAppIndexDashboard'
ADMIN_TOOLS_THEMING_CSS = 'static/css/custom.css'


REST_FRAMEWORK = {

    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
        #permission class will deny permission to any user, unless user.is_staff is True
        #in which case permission will be allowed.
        'rest_framework.permissions.IsAdminUser',
        #will allow authenticated users to perform any request.
       # Requests for unauthorised users will only be permitted
       #if the request method is one of the "safe" methods; GET, HEAD or OPTIONS,
       'rest_framework.permissions.IsAuthenticatedOrReadOnly'
    ],

    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ),
    'DEFAULT_THROTTLE_RATES': {
    #maybe second/minute /hour/day
     #   'anon': '100/day',
       # 'user': '1000/day'
    },

    'PAGE_SIZE': 10
}




