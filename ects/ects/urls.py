"""ects URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.http import HttpResponseRedirect

from case.views import OffenceAutoComplete
from case.views import StationAutoComplete
from case.views import OffenceCategoryAutoComplete
from case.views import InvestigatorAutoComplete,DocketAutoComplete
from case.views import AttorneyAutoComplete
from case.views import test1pdfview



#admin actions
from django.contrib.admin import site
import adminactions.actions as actions

# register all adminactions
actions.add_to_site(site)

urlpatterns = [
      url(r'^', include(admin.site.urls)),

    #url(r'^$', include(admin.site.urls')),
    #redirect to index to admin
 #url(r'^$', HttpResponseRedirect('admin/')),
    url(r'^$', include(admin.site.urls)),
   # url(r'^/case$', include(admin.site.urls)),

    url(r'^admin_tools/', include('admin_tools.urls')),
   url(r'^admin/', admin.site.urls),

    url(r'^case/', include('case.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^testpdf$', test1pdfview),

    #autocomplete urls
    url(r'^offencecategory-autocomplete/$',OffenceCategoryAutoComplete.as_view(),name='offencecategory-autocomplete'),
    url(r'^offence-autocomplete/$',OffenceAutoComplete.as_view(),name='offence-autocomplete'),
    url(r'^station-autocomplete/$',StationAutoComplete.as_view(),name='station-autocomplete'),
    url(r'^investigator-autocomplete/$',InvestigatorAutoComplete.as_view(),name='investigator-autocomplete'),
    url(r'^docket-autocomplete/$',DocketAutoComplete.as_view(),name='docket-autocomplete'),
    url(r'^attorney-autocomplete/$',AttorneyAutoComplete.as_view(),name='attorney-autocomplete'),





]

urlpatterns += staticfiles_urlpatterns()
