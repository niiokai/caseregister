from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Profile

class ProfileInline(admin.StackedInline):
    model =Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'

    #list_display = []


class CustomUserAdmin(UserAdmin):
    '''
    normaluser_fields = ('available',)
    superuser_fields = ('name', 'serialno',)

    def get_form(self, request, obj=None, **kwargs):
       if request.user.is_superuser:
           self.fields = self.normaluser_fields + self.superuser_fields
       else:
           self.fields = self.normaluser_fields
        return super(CustomerUserxAdmin, self).get_form(request, obj, **kwargs)
    '''
    inlines = (ProfileInline,)
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'get_station')
    list_select_related = ('profile', )

    def get_station(self,instance):
        return instance.profile.station

    get_station.short_description  = 'Station'

    def get_inlines(self,request,obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin,self).get_inline_instances(request,obj)


admin.site.unregister(User)
admin.site.register(User,CustomUserAdmin)
