from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from case.models import Region
from utils import CharNullField

class Profile(models.Model):
    SECRETARY = 1
    EXECUTIVE_OFFICER = 2
    ADMINISTRATIVE_OFFICER = 3
    STATE_ATTORNEY = 4
    REGIONAL_HEAD = 5
    DPP = 6

    ROLE_CHOICES = (
            (SECRETARY ,'Secretary'),
            (EXECUTIVE_OFFICER ,'Executive Officer'),
           (ADMINISTRATIVE_OFFICER , 'Administrative Officer'),
            (STATE_ATTORNEY , 'State Attourney'),
            (REGIONAL_HEAD , 'Regional Head'),
            (DPP,'Director of Public Prosecution'),

        )

    user  = models.OneToOneField(User,on_delete=models.CASCADE)
    staff_id = CharNullField(max_length=30,blank=True,unique=True,null=True)
    station  =   models.ForeignKey(Region, on_delete=models.CASCADE,null=True,blank=True)
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES,null=True,blank=True)

    def __str__(self):
        return self.user.username

@receiver(post_save,sender=User)
def create_or_update_user_profile(sender,instance, created,**kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()




