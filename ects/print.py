from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse , HttpResponseNotFound

def pdf_view(request):
    fs = FileSystemStorage()
    filename = 'myname.pdf'

    if fs.exists(filename):
        with fs.open(filename) as  pdf:
            response =  HttpResponse(pdf,content_type 'application/pdf')
            #display finel in browser
            #response['Content-Disposition'] = 'inline; filename="mypdf.pdf"'
            #open file for saving
            response['Content-Disposition'] = 'attachement; filename= "myfile.pdf"'
            return response
    else:
        return HttpResponseNotFound('The requested pdf was not found')
