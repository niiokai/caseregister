from django.contrib.admin.sites import AdminSite

class AdminMixin(object):

    def __init__(self,*args,**kwargs):
        return super(AdminMixin,*args,**kwargs)

     def get_urls(self):
        #add our dashboard view to admin url delete default index
        from django.conf.urls import  patterns ,url
        from views import  DashboardWelcomeView
        urls     = super(AdminMixin,self).get_urls()
        del urls[0]


        custom_url  =  patterns('',
            url(r'ˆ$', self.admin_view(DashboardWelcomeView,as_view()),
                name='index')
            )

            return custom_url + urls
